```
npm install
npm run build
```

## TODO

Using 2-3 hours, please tackle as many of these as you wish:

- ✓ Render list of cakes
- ✓ Update to pass eslint (http://eslint.org/) to run: eslint src
- ✓ Implement a loading state
- ✓ Investigate using the Flux (Flux or Redux implentation) pattern with a CakeStore and dispatcher
- ✓ Make UI render nicely on iPad and Nexus 5
- Add ability to search cakes
- ✓ Add basic ability to edit/add cakes
- ✓ Add tests

## Notes

- The test had to be first updated to use react 0.14.7 which also involved replacing react-tools with babel
- To provide a more fluid and customisable build pipleine, I replaced bower with gulp and browserify to allow me to bundle all modules and dependencies and serve only a single bundle.js file to the browser. While there are ways to do this with bower, tooling in the JS and React community seems to be better suited to using gulp, grunt or webpack over bower.
- The implementation of Redux leaves room for a lot of improvement. Ideally the store should not be passed down the component tree as a prop. Implementing callbacks for the action dispatchers on the components that need them and then using connect() from react-redux is one possible solution.
- Test coverage is not as strong as it should be and the style of testing is inconsistient. Ideally there would be more use of test helper utility methods and a more consistient approach to what is tested and how.
- The folder structure was a best-guess, were the app more complex I would be looking to use a more 'feature' oriented layout with components, containers, reducers, etc. under their approprate feature groups.
