var gulp = require('gulp');
var babelify = require('babelify');
var browserify = require('browserify');
var source = require('vinyl-source-stream');

gulp.task('default', ['build']);

gulp.task('build', function(){
    return browserify({
        entries: ['./src/app.js'],
        extensions: [".js", ".jsx", '.es6']
    })
    .transform(babelify.configure({
        presets: ['es2015', 'react']
    }))
    .bundle()
    .pipe(source('bundle.js'))
    .pipe(gulp.dest('./build'));
});