export function addCake(cake) {
  return {
    type: 'ADD_CAKE',
    title: cake.title,
    image: cake.image
  };
};