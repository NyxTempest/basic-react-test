jest.unmock('../CakeActions');

import {addCake} from '../CakeActions';

describe('addCakes', () => {
  it('creates an action for adding a cake', () => {
    const cake = {
      title: 'Really good noms',
      image: 'noms.jpg'
    };

    const expectedAction = {
      type: 'ADD_CAKE',
      title: cake.title,
      image: cake.image
    };

    expect(addCake(cake)).toEqual(expectedAction);
  });
});