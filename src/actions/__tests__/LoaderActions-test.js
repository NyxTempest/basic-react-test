jest.unmock('../LoaderActions');

import {loaded} from '../LoaderActions';

describe('loaded', () => {
  it('creates an action for when cakes are loaded', () => {
    const expectedAction = {
      type: 'CAKES_LOADED'
    };

    expect(loaded()).toEqual(expectedAction);
  });
});