import React from 'react';
import {addCake} from '../actions/CakeActions';
import {loaded} from '../actions/LoaderActions';

export default class AddCake extends React.Component {
  constructor(props) {
    super(props);
    this.store = this.props.store;
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(e) {
    e.preventDefault();

    this.store.dispatch(addCake({
      title: this.title.value,
      desc: this.desc.value,
      image: this.image.value
    }));
    this.store.dispatch(loaded());
    this.clearForm();
  }
  
  clearForm() {
    this.title.value = '',
    this.desc.value = '',
    this.image.value = '';
  }

  render() {
    const formGroupStyle = {
      marginRight: '5px'
    };

    const inputStyle = {
      marginLeft: '5px'
    };

    return <form className='form-inline' onSubmit={this.handleSubmit}>
      <div className='form-group' style={formGroupStyle}>
        <label htmlFor='newCakeTitle'>Title</label>
        <input type='text' ref={(c) => this.title = c} className='form-control' id='newCakeTitle' style={inputStyle} />
      </div>
      <div className='form-group' style={formGroupStyle}>
        <label htmlFor='newCakeDesc'>Description</label>
        <input type='text' ref={(c) => this.desc = c} className='form-control' id='newCakeDesc' style={inputStyle} />
      </div>
      <div className='form-group' style={formGroupStyle}>
        <label htmlFor='newCakeImage'>Image</label>
        <input type='text' ref={(c) => this.image = c} className='form-control' id='newCakeImage' style={inputStyle} />
      </div>
      <button className='btn btn-primary' aria-label='Add Cake'>
        <span className="glyphicon glyphicon-plus" aria-hidden="true"></span>
      </button>
    </form>;
  }
};
