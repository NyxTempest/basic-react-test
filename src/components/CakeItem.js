import React from 'react';

export default class CakeItem extends React.Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    var imgStyle = {
      maxHeight: '100px'
    };

    return(
      <li className='media list-group-item'>
        <div className='media-left media-top'>
          <img className='media-object' src={this.props.cake.image} style={imgStyle} />                    
        </div>
        <div className='media-body'>
          <h4>{this.props.cake.title}</h4>
        </div>
      </li>
    );
  }    
};