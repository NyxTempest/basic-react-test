import React from 'react';
import CakeItem from './CakeItem';
import AddCake from './AddCake';

export default class CakeList extends React.Component {
  constructor(props) {
    super(props);     
    this.store = props.store;   
  }  

  render() {
    return <div>
      <h1>Cakes!</h1>
      
      <AddCake store={this.store} />
      
      <ul className='media-list list-group'> {
        this.props.cakes.map((cake, i) => { 
          return <CakeItem cake={cake} key={i} />; 
        })
      }
      </ul>
    </div>;
  }
};
