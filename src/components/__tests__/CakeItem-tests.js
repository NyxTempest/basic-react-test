jest.unmock('../CakeItem');

import React from 'react';
import TestUtils from 'react-addons-test-utils';
import CakeItem from '../CakeItem';

describe('CakeItem', () => {
  it('renders a list item describing the cake', () => {
    let initialCake = {
      title: 'Really good noms',
      image: 'noms.jpg'
    };

    let renderer = TestUtils.createRenderer();
    renderer.render(<CakeItem cake={initialCake} />);

    let result = renderer.getRenderOutput();

    expect(result).toEqual(
      <li className='media list-group-item'>
        <div className='media-left media-top'>
          <img className='media-object' src='noms.jpg' style={{ maxHeight: '100px' }} />
        </div>
        <div className='media-body'>
          <h4>Really good noms</h4>
        </div>
      </li>
    );
  });
});