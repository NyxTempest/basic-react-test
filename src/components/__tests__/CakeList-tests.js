jest.unmock('../CakeList');

import React from 'react';
import TestUtils from 'react-addons-test-utils';
import {findAllWithType} from 'react-shallow-testutils';
import {findWithType} from 'react-shallow-testutils';

import CakeList from'../CakeList';
import CakeItem from'../CakeItem';
import AddCake from'../AddCake';

describe('CakeList', () => {
  it('renders a an unordered list of CakeItem', () => {
    const cakes = [
      {
        title: 'Really good noms',
        image: 'noms.jpg'
      },
      {
        title: 'Excellent noms',
        image: 'omnomnom.jpg'
      },
      {
        title: 'The best noms',
        image: 'omgnom.jpg'
      }
    ];

    let renderer = TestUtils.createRenderer();
    renderer.render(<CakeList cakes={cakes} />);

    let result = renderer.getRenderOutput();

    let cakeItems = findAllWithType(result, CakeItem);
    expect(cakeItems.length).toBe(3);

    let cake = cakeItems[1];
    expect(cake).toEqual(<CakeItem key='1' cake={cakes[1]} />);
  });
  
  it('render an AddCake component', () => {
    let renderer = TestUtils.createRenderer();
    renderer.render(<CakeList cakes={[]} />);
    
    var getOutput = () => findWithType(renderer.getRenderOutput(), AddCake);
    expect(getOutput).not.toThrow();
  });
});