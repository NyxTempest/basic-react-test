import React from 'react';
import Loader from 'react-loader';
import $ from 'jquery';
import CakeList from '../components/CakeList';
import {addCake} from '../actions/CakeActions';
import {loaded} from '../actions/LoaderActions';

const CAKE_URL = 'https://gist.githubusercontent.com/hart88/198f29ec5114a3ec3460/raw/8dd19a88f9b8d24c23d9960f3300d0c917a4f07c/cake.json';

export default class CakeListContainer extends React.Component {    
  constructor(props) {
    super(props); 
    this.store = this.props.store;
    this.state = { loaded: this.store.getState().loaded };
  }
  
  componentWillMount() {
    this.store.subscribe(this.render.bind(this));
    this.store.subscribe(this.setLoaded.bind(this));;
  }
  
  componentDidMount() {
    $.ajax({
      url: CAKE_URL,
      success: function(response) {
        window.setTimeout(() => { // Simulating a delay in loading to demonstrate the loader
          var cakes = JSON.parse(response);
          cakes.map((cake) => {
            this.store.dispatch(addCake(cake));
          });       
          this.store.dispatch(loaded());   
        }, 3000);
      }.bind(this)
    });
  }
  
  setLoaded() {
    this.setState({loaded: this.store.getState().loaded});
  }

  render() {
    return (
      <Loader loaded={this.state.loaded}>       
        <CakeList cakes={this.store.getState().cakes } store={this.store} />
      </Loader>
    );
  }
};