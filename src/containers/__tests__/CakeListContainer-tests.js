jest.unmock('redux');
jest.unmock('../CakeListContainer');
jest.unmock('../../components/CakeList');
jest.unmock('../../reducers/Cake');
jest.unmock('../../reducers/Loader');
jest.unmock('../../reducers/Index');

import React from 'react';
import ReactDOM from 'react-dom';
import {createStore} from 'redux';
import {findWithType} from 'react-shallow-testutils';
import TestUtils from 'react-addons-test-utils';
import Loader from 'react-loader';
import $ from 'jquery';

import CakeListContainer from '../CakeListContainer';
import CakeList from '../../components/CakeList';
import Reducer from '../../reducers/Index';

describe('CakeListContainer', () => {
  let store = {};

  beforeEach(() => {
    store = createStore(Reducer);
  });

  it('gets cakes from API', () => {
    TestUtils.renderIntoDocument(<CakeListContainer store={store} />);

    expect($.ajax).toBeCalledWith({
      url: 'https://gist.githubusercontent.com/hart88/198f29ec5114a3ec3460/raw/8dd19a88f9b8d24c23d9960f3300d0c917a4f07c/cake.json',
      success: jasmine.any(Function)
    });
  });

  it('renders a CakeList', () => {
    const renderer = TestUtils.createRenderer();
    renderer.render(<CakeListContainer store={store} />);

    var getOutput = () => findWithType(renderer.getRenderOutput(), CakeList);
    expect(getOutput).not.toThrow();
  });

  it('initially renders unloaded', () => {
    const renderer = TestUtils.createRenderer();
    renderer.render(<CakeListContainer store={store} />);

    var result = renderer.getRenderOutput();
    var loader = findWithType(renderer.getRenderOutput(), Loader);

    expect(loader.props.loaded).toBe(false);
  });

  it('hides the loader when cakes have been loaded', () => {
    let container = TestUtils.renderIntoDocument(<CakeListContainer store={store} />);
    container.setState({loaded: true});

    var loader = TestUtils.findRenderedComponentWithType(container, Loader);
    expect(loader.state.loaded).toBe(true);
  });
});