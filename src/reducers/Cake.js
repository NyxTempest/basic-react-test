export default function cakes(state = [], action) {
  /*eslint indent: [2, 2, {"SwitchCase": 1}]*/
  switch (action.type) {
    case 'ADD_CAKE':
      return [        
        {
          title: action.title,
          image: action.image
        },   
        ...state     
      ];
    default: 
      return state;
  }
};