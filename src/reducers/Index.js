import { combineReducers } from 'redux';
import Cake from './Cake';
import Loader from './Loader';

const cakeStore = combineReducers({
  cakes : Cake,
  loaded : Loader
});

export default cakeStore;