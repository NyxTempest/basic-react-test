export default function loader(state = false, action) {
  switch (action.type) {
  case 'CAKES_LOADED':
    return true;
  default:
    return false;
  }
};