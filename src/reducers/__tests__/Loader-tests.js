jest.unmock('../Loader');
jest.unmock('../../actions/LoaderActions');

import {loaded} from '../../actions/LoaderActions';
import Loader from '../Loader';

describe('Loader', () => {
  it('should return the inital state', () => {
    expect(Loader(undefined, {}))
      .toEqual(false);
  });

  it('should handle CAKES_LOADED', () => {
    expect(Loader([], loaded()))
      .toEqual(true);
  });
});